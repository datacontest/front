# Tasks

* Persist state during development hot reload! (if the url is different, it doesnt work)

* refresh token middleware?

* Login:
  - show error on invalid username/password
  - show error if some of those aren't seted onSubmit
  - show "already logged in" or some CTA in the form if already logged

* Create user (both api and front)

* DatathonList in dashboard

* [In progress] AddNewDatathonForm
  - correctly show response errors
  - validate csv's
  - (future) only one file as dataset!

* Dashboard do not require login, but AddNewDatathonForm does
