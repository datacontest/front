import React from 'react'
import { connect } from 'react-redux'
import Papa from 'papaparse'

import { addDatathon } from '../actions/datathonActions'


class AddDatathonForm extends React.Component {
  /*
   TODO:
    - validate all csvs have the same headers
    - more than N rows?
    - validate target is in headers
    - !! use bootstrap helpers to show related errors in every form field
  */

  constructor(props) {
    super(props)

    this.state = {
      formValues: {
        title: "",
        description: "",
        metric: 'ACCURACY',
        dataset_description: "",
        dataset_target_variable_name: "",
        dataset_training_set: null,
        dataset_test_set: null,
        dataset_validation_set: null,
      },
      csvErrors: {
        dataset_training_set: null,
        dataset_test_set: null,
        dataset_validation_set: null
      },
      hasSubmited: false,
    }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleFileChange = this.handleFileChange.bind(this)
    this.handleCompleteCsvParse = this.handleCompleteCsvParse.bind(this)
  }

  handleChange(event) {
    this.setState(
      Object.assign({},
        this.state,
        { formValues: Object.assign({}, this.state.formValues, { [event.target.id]: event.target.value }) }
      )
    )
  }

  handleSubmit(event) {
    event.preventDefault()

    this.setState({ hasSubmited: true })
    this.props.dispatch(
      addDatathon({...this.state.formValues})
    )
  }

  handleCompleteCsvParse(targetId, results, file) {
    if (results.errors.length != 0) {
      this.setState({
        csvErrors: {
          ...this.state.csvErrors,
          [targetId]: "There have been some errors parsing the file. View the console log :("
        }
      })
      console.log(`Errors parsing file ${targetId}`, results.errors)
    } else {
      const target_feature = this.state.formValues.dataset_target_variable_name
      const csvHeaders = results.meta.fields
      const targetInHeaders = csvHeaders.includes(target_feature)
      if (target_feature != "" && !targetInHeaders) {
        this.setState({
          csvErrors: {
            ...this.state.csvErrors,
            [targetId]: `We couldn't find the target feature (${target_feature}) in the headers: ${csvHeaders.join(',')}`
          }
        })
      } else {
        this.setState({
          csvErrors: {
            ...this.state.csvErrors,
            [targetId]: null
          }
        })
      }
    }
  }

  handleFileChange(event) {
    const target = event.target
    const eventFile = target.files[0]

    if (eventFile.type == "text/csv") {
      this.setState({ csvErrors: { ...this.state.csvErrors, [target.id]: null }})

      Papa.parse(eventFile, {
        header: true,
        delimiter: ',',
        skipEmptyLines: true,
        error: function(error, file) {
          console.log(`TODO Error parsing the file ${target.id}`)
          console.log(error)
        },
        complete: (results, file) => this.handleCompleteCsvParse(target.id, results, file)
      })
    } else {
        this.setState({
          csvErrors: {
            ...this.state.csvErrors,
            [target.id]: "Invalid file type: " + eventFile.type
          }
        })
    }

    this.setState({
      formValues: {
        ...this.state.formValues,
        [event.target.id]: eventFile
      }
    })
  }

  render() {
    const {
      csvErrors,
      hasSubmited,
      formValues: { title, description, dataset_description, dataset_target_variable_name }
    } = this.state

    // TODO show humanized option
    const metricOptions = {
      'ACCURACY': 'Accuracy',
      'AUC': 'Area Under the ROC Curve',
      'MSE': 'Mean Squared Error',
      'MAE': 'Mean Absolute Error'
    }

    let errorMsg = <div></div>
    if (this.props.errors) {
      errorMsg = Object.keys(this.props.errors).map(key => {
        if (key == 'dataset') {
          console.log("ERRORS DEL DATASET:", this.props.errors['dataset'])
          return Object.keys(this.props.errors[key]).map(dataset_error =>
            <li key={dataset_error.toString()}>
              <strong>dataset {dataset_error}</strong>: {this.props.errors[key][dataset_error]}
            </li>
          )
        }
        return <li key={key.toString()}><strong>{key}</strong>: {this.props.errors[key]}</li>
      })
    }

    const obtainError = (key) => {
      return errorKeys.includes(key) ? this.props.errors[key].join(',') : ''
    }

    const obtainDatasetError = (key) => {
      if (errorKeys.includes('dataset') && Object.keys(this.props.errors['dataset']).includes(key)) {
        return this.props.errors['dataset'][key].join(',')
      }
      return ''
    }

    const errorKeys = (hasSubmited && this.props.errors) ? Object.keys(this.props.errors) : []
    const titleError = obtainError('title')
    const descriptionError = obtainError('description')
    const metricError = obtainError('metric')
    const datasetDescriptionError = obtainDatasetError('description')
    const datasetTargetError = obtainDatasetError('target_variable_name')

    // TODO errors on the target files can be both from server side and from the handleChange
    // when correctly handled, remove the errorMsg!
    console.log("Errors: ", this.props.errors)
    console.log(datasetDescriptionError)

    return (
      <div className="container">
        <h1>Host a datathon</h1>

        {hasSubmited &&
          <span>{errorMsg}</span>
        }

        <form onSubmit={this.handleSubmit}>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="title">Title</label>
            <div className="col-sm-10">
              <input
                type="text"
                className={`form-control ${titleError ? 'is-invalid': ''}`}
                id="title"
                value={title}
                onChange={this.handleChange}
              />
              {titleError &&
                <div className="invalid-feedback">{titleError}</div>
              }
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="description">Description</label>
            <div className="col-sm-10">
              <textarea
                type="text"
                rows="4"
                className={`form-control ${descriptionError ? 'is-invalid': ''}`}
                id="description"
                value={description}
                onChange={this.handleChange}
              />
              {descriptionError &&
                <div className="invalid-feedback">{descriptionError}</div>
              }
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="metric">Metric</label>
            <div className="col-sm-3">
              <select
                className={`form-control ${metricError ? 'is-invalid' : ''}`}
                onChange={this.handleChange}
                id="metric"
              >
                {Object.keys(metricOptions).map((key) => (
                  <option key={key}>{key}</option>
                ))}
              </select>
              {metricError &&
                <div className="invalid-feedback">{metricError}</div>
              }
            </div>
          </div>
          <h3>Dataset:</h3>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="dataset_description">Description</label>
            <div className="col-sm-10">
              <textarea
                type="text"
                rows="3"
                className={`form-control ${datasetDescriptionError ? 'is-invalid': ''}`}
                id="dataset_description"
                value={dataset_description}
                onChange={this.handleChange}
              />
              {datasetDescriptionError &&
                <div className="invalid-feedback">{datasetDescriptionError}</div>
              }
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="dataset_target_variable_name">Target feature</label>
            <div className="col-sm-4">
            <input
              type="text"
              className={`form-control ${datasetTargetError ? 'is-invalid': ''}`}
              id="dataset_target_variable_name"
              value={dataset_target_variable_name}
              placeholder="Header of the column to predict"
              onChange={this.handleChange}
            />
            {datasetTargetError &&
              <div className="invalid-feedback">{datasetTargetError}</div>
            }
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="dataset_training_set">Training set</label>
            <div className="col-sm-10">
            <input
              type="file"
              className={`form-control-file ${csvErrors.dataset_training_set ? 'is-invalid' : ''}`}
              id="dataset_training_set"
              onChange={this.handleFileChange}
            />
            {csvErrors.dataset_training_set &&
              <div className="invalid-feedback">{csvErrors.dataset_training_set}</div>
            }
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="dataset_test_set">Test set</label>
            <div className="col-sm-10">
            <input
              type="file"
              className={`form-control-file ${csvErrors.dataset_test_set ? 'is-invalid' : ''}`}
              id="dataset_test_set"
              onChange={this.handleFileChange}
            />
            {csvErrors.dataset_test_set &&
              <div className="invalid-feedback">{csvErrors.dataset_test_set}</div>
            }
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label col-form-label-lg" htmlFor="dataset_validation_set">Validation set</label>
            <div className="col-sm-10">
            <input
              type="file"
              className={`form-control-file ${csvErrors.dataset_validation_set ? 'is-invalid' : ''}`}
              id="dataset_validation_set"
              onChange={this.handleFileChange}
            />
            {csvErrors.dataset_validation_set &&
              <div className="invalid-feedback">{csvErrors.dataset_validation_set}</div>
            }
            </div>
          </div>

          <button type="submit" className="btn btn-success btn-lg">
            Go!
          </button>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return { ...state.datathonsReducer.creation }
}

export default connect(mapStateToProps)(AddDatathonForm)
