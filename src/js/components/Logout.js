import React from 'react'
import { connect } from 'react-redux'
import { logoutUser } from '../actions/sessionActions'



class Logout extends React.Component {
  componentDidMount() {
    this.props.dispatch(logoutUser())
  }

  render() {
    return (
      <div>Bye bye</div>
    )
  }
}

export default connect()(Logout)
