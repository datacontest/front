import React from 'react'
import { Link } from 'react-router-dom'

import moment from 'moment'


const DatathonPreview = ({ id, title, description, created_at }) => (
  <Link
    key={id}
    to={"/datathons/" + id}
    className="list-group-item list-group-item-action flex-column align-items-start">
    <strong>{title}</strong>

    <div className="d-flex w-100 justify-content-between">
      <h5 className="mb-1">{title}</h5>
      <small>{moment(created_at).fromNow()}</small>
    </div>
    <p className="mb-1">{description}</p>
    <small>Donec id elit non mi porta.</small>
  </Link>
)

const DatathonList = ({ datathons }) => (
  <div className="list-group" style={{width: '100%'}}>
    {Object.keys(datathons).map(datathon_id => (
      <DatathonPreview key={datathon_id} {...datathons[datathon_id]} />
    ))}
  </div>
)

export default DatathonList
