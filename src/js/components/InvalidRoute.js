import React from "react"

const InvalidRoute = () => (
  <div className="container">
    404 - not found
  </div>
)

export default InvalidRoute
