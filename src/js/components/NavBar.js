import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

const NavBar = ({ isAuthenticated, username }) => (
  <nav className="navbar navbar-expand-md navbar-dark bg-dark">
    <Link className="navbar-brand" to="/">Datacontest</Link>

    {isAuthenticated &&
    <ul className="navbar-nav ml-auto">
      <li className="nav-item"><Link className="nav-link" to="/dashboard">Dashboard</Link></li>
      <li className="nav-item dropdown">
        <a className="nav-link dropdown-toggle" href="#" id="navbarUserDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span className="oi oi-person" title="person" aria-hidden="true"></span> {username}
        </a>
        <div className="dropdown-menu" aria-labelledby="navbarUserDropdown">
          <Link className="dropdown-item" to="/user">User Settings</Link>
          <Link className="dropdown-item" to="/logout">Logout</Link>
        </div>
      </li>
      <li className="nav-item">
            <Link to="/add_datathon">
              <button className="btn btn-success btn-md">
                Host a datathon
              </button>
            </Link>
      </li>
    </ul>
    }
    {isAuthenticated ||
    <ul className="navbar-nav ml-auto">
      <li className="nav-item"><Link className="nav-link" to="/dashboard">Dashboard</Link></li>
      <li className="nav-item"><Link className="nav-link" to="/login">Login</Link></li>
      <li className="nav-item"><Link className="nav-link" to="/signup">Signup</Link></li>
    </ul>
    }
  </nav>
)

const mapStateToProps = state => {
  const { authReducer: { isAuthenticated, user: { username } } } = state
  return {
    isAuthenticated,
    username
  }
}

export default connect(mapStateToProps)(NavBar)
