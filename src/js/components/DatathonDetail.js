import React from 'react'
import { connect } from 'react-redux'

import { retrieveDatathon } from '../actions/datathonActions'


const DatathonCard = ({ title, description, isOrganizer }) => (
  <div className="datathonCard">
    <h1>{title} {isOrganizer && <span className="badge badge-secondary">Hosted by you</span>}</h1>
    <div className="card bg-light mb-3">
      <div className="card-header"><strong>Description</strong></div>
      <div className="card-body">
        <p className="card-text">{description}</p>
      </div>
    </div>
  </div>
)

const DatathonSubmissionForm = ({ id }) => (
  <div className="submissionForm">
    <div className="card bg-light mb-3">
      <div className="card-header"><strong>Submission</strong></div>
      <div className="card-body">
        <form>
          <div className="custom-file">
            <input type="file" className="custom-file-input" id="customFile" />
            <label className="custom-file-label" htmlFor="customFile">Choose file</label>
          </div>
        </form>
      </div>
    </div>

    A form to send submissions to datathon {id}
  </div>
)


const DatathonScoreboard = ({ id }) => (
  <div className="datathonScoreboard">
    <div className="card text-white bg-dark mb-3">
      <div className="card-header"><strong>Scoreboard</strong></div>
      <div className="card-body">
        <p className="card-text">List of results of datathon {id}</p>
      </div>
    </div>
  </div>
)

const DatathonHeader = ({ id, title, description, isOrganizer, isAuthenticated, isParticipant }) => (
  <div className="datathonHeader">
    <div className="">
     Fucking header
    </div>
  </div>
)

class DatathonDetail extends React.Component {
  componentDidMount() {
    const datathonId = this.props.match.params.id
    this.props.dispatch(retrieveDatathon(datathonId))
  }

  render() {
    // TODO aixo es un desastre, granularitzar en diferents contenidors
    // separant conceptes i repensant el pas de props
    const { isAuthenticated, user, datathon } = this.props
    const { id, title, description, participants, organizer } = datathon

    const isOrganizer = isAuthenticated ? organizer == user.username : false
    const isParticipant = isAuthenticated ? participants.map( (u) => (u.username) ).includes(user.username) : false

    return (
      <div className="container">
        <div className="row">
          <div className="col-8">
            <DatathonHeader
              {...datathon}
              isAuthenticated={isAuthenticated}
              isOrganizer={isOrganizer}
            />
            <DatathonCard {...datathon} isOrganizer={isOrganizer} />


            {isOrganizer &&
              <div>This should be organizer section!</div>
            }

            {isParticipant &&
              <DatathonSubmissionForm id={id} />
            }
          </div>
          <div className="col-4">
            <DatathonScoreboard id={id} />
          </div>
        </div>
      </div>
    )
  }
}


function mapStateToProps(state, ownProps) {
  const reducer = state.datathonsReducer

  const datathon = reducer.datathons[ownProps.match.params.id]

  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    user: state.authReducer.user,
    isFetching: reducer.retrieval.isFetching,
    datathon,
  }
}

export default connect(mapStateToProps)(DatathonDetail)
