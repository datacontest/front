import React from 'react'
import { connect } from 'react-redux'

import { retrieveDatathons } from '../actions/datathonActions'
import DatathonList from './DatathonList'


class Dashboard extends React.Component {
  componentDidMount() {
    this.props.dispatch(retrieveDatathons())
  }

  render() {
    const { isFetching, datathons } = this.props

    return (
      <div className="container">
        <div className="row">
          <h1>Datathon list</h1>
        </div>

        <div className="row">
          {isFetching && <div>Is fetching!</div>}
          <DatathonList datathons={datathons} />
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    isFetching: state.datathonsReducer.retrieval.isFetching,
    datathons: state.datathonsReducer.datathons,
  }
}

export default connect(mapStateToProps)(Dashboard)
