import { push } from 'connected-react-router'

import {
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  LOGIN_REQUEST,
  LOGOUT
} from '../constants/actionTypes'
import SessionApi from '../api/sessionApi'

export function loginRequest() {
  return { type: LOGIN_REQUEST }
}

export function loginSuccess(user) {
  return { type: LOGIN_SUCCESS, user }
}

export function loginError(errorMsg) {
  return { type: LOGIN_FAILURE, errorMsg }
}

export function logout() {
  return { type: LOGOUT }
}

/**
 * Logs an user in
 * @param  {string} username The username of the user to be logged in
 * @param  {string} password The password of the user to be logged in
 */
export function loginUser(username, password) {
  return dispatch => {
    dispatch(loginRequest())
    SessionApi.login(username, password).then(response => {
      localStorage.setItem('jwt_token', response.token)
      dispatch(loginSuccess(response.user))
      dispatch(push('/dashboard'))
    }).catch(error => {
      console.log("Error logging the user:")
      console.log(error)
      dispatch(loginError(error.message))
    })
  };
}

export function refreshToken() {
  const token = localStorage.getItem('jwt_token')
  return dispatch => {
    SessionApi.refreshToken(token).then(response => {
      
    })
  }
}

export function logoutUser() {
  return dispatch => {
    localStorage.removeItem('jwt_token')
    dispatch(logout())
  }
}