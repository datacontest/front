import { push } from 'connected-react-router'

import {
  RETRIEVE_DATATHONS,
  RETRIEVE_DATATHONS_SUCCESS,
  RETRIEVE_DATATHONS_FAILURE,
  RETRIEVE_DATATHON,
  RETRIEVE_DATATHON_SUCCESS,
  RETRIEVE_DATATHON_FAILURE,
  ADD_DATATHON,
  ADD_DATATHON_SUCCESS,
  ADD_DATATHON_FAILURE
} from '../constants/actionTypes'

import DatathonApi from '../api/datathonApi'

function requestDatathonList() {
  return { type: RETRIEVE_DATATHONS }
}

function datathonListRequestSuccess(datathons) {
  return { type: RETRIEVE_DATATHONS_SUCCESS, datathons }
}

function datathonListRequestError(errorMsg) {
  return { type: RETRIEVE_DATATHONS_FAILURE, errorMsg }
}

export function retrieveDatathons() {
  return dispatch => {
    dispatch(requestDatathonList())
    DatathonApi.getAll().then(response => {
      dispatch(datathonListRequestSuccess(response))
    }).catch(error => {
      dispatch(datathonListRequestError(error.message))
    })
  }
}

function requestDatathon(id) {
  return { type: RETRIEVE_DATATHON, id }
}

function datathonRequestSuccess(datathon) {
  return { type: RETRIEVE_DATATHON_SUCCESS, datathon }
}

function datathonRequestError(error) {
  return { type: RETRIEVE_DATATHON_FAILURE, error }
}

export function retrieveDatathon(id) {
  return dispatch => {
    dispatch(requestDatathon(id))
    DatathonApi.get(id).then(response => {
      dispatch(datathonRequestSuccess(response))
    }).catch(error => {
      dispatch(datathonRequestError(error))
    })
  }
}

function addingDatathon() {
  return { type: ADD_DATATHON }
}

function addDatathonSuccess(datathon) {
  return { type: ADD_DATATHON_SUCCESS, datathon }
}

function addDatathonFailure(errors) {
  return { type: ADD_DATATHON_FAILURE, errors }
}

export function addDatathon(fields) {
  return dispatch => {
    dispatch(addingDatathon())
    DatathonApi.add(fields).then(response => {
      dispatch(addDatathonSuccess(response))
      dispatch(push('/datathons/' + response.id))
    }).catch(errors => {
      dispatch(addDatathonFailure(errors))
    })
  }
}