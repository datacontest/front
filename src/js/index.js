import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'

import { AppContainer } from 'react-hot-loader'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import { applyMiddleware, compose, createStore } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage' // defaults to localStorage
import thunk from 'redux-thunk'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'open-iconic/font/css/open-iconic-bootstrap.css'
import rootReducer from "./reducers/index"
import App from "./App"

const persistConfig = {
  key: 'root',
  storage,
}

const history = createBrowserHistory()

const persistedReducer = persistReducer(persistConfig, connectRouter(history)(rootReducer))
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  persistedReducer,
  composeEnhancer(
    applyMiddleware(
      routerMiddleware(history),
      thunk
    ),
  ),
)
const persistor = persistStore(store)

render(
  <AppContainer>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <App history={history} />
      </PersistGate>
    </Provider>
  </AppContainer>,
  document.getElementById("app")
);


// Hot reloading
if (module.hot) {
  // Reload components
  module.hot.accept('./App', () => {
    render()
  })

  // Reload reducers
  module.hot.accept('./reducers', () => {
    store.replaceReducer(connectRouter(history)(rootReducer))
  })
}
