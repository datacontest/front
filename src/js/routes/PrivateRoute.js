import React from 'react'
import { connect } from "react-redux"
import { Route, Redirect } from 'react-router'


const PrivateRoute = ({ component: Component, ...props }) => (
  <Route {...props} render={() => (
    props.isAuthenticated ? (
      <Component {...props} />
    ) : (
      <Redirect to="/login" />
    )
  )} />
)

const mapStateToProps = (state, ownProps) => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    ...ownProps
  }
}

export default connect(mapStateToProps, null, null, {
  pure: false
})(PrivateRoute)
