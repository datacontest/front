import React from 'react'
import { Route, Switch } from 'react-router'

import PrivateRoute from './PrivateRoute'
import AddDatathonForm from '../components/AddDatathonForm'
import Dashboard from '../components/Dashboard'
import DatathonDetail from '../components/DatathonDetail'
import Home from '../components/Home'
import InvalidRoute from '../components/InvalidRoute'
import Login from '../components/Login'
import Logout from '../components/Logout'
import NavBar from '../components/NavBar'
import UserDetail from '../components/UserDetail'


// TODO PrivateRoute should display a msg after redirecting to the login form,
//      something like "you should be logged in to host a datathon".
// TODO Logout should require to be logged in.

const routes = (
  <div>
    <NavBar />
    <div style={{marginTop: '50px'}}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/login" component={Login} />
        <Route path="/logout" component={Logout} />
        <Route path="/dashboard" component={Dashboard} />
        <Route path="/datathons/:id" component={DatathonDetail} />
        <PrivateRoute path="/user" component={UserDetail} />
        <PrivateRoute path="/add_datathon" component={AddDatathonForm} />
        <Route component={InvalidRoute} />
      </Switch>
    </div>
  </div>
)

export default routes
