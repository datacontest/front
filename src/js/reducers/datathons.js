import {
  ADD_DATATHON,
  ADD_DATATHON_FAILURE,
  ADD_DATATHON_SUCCESS,
  RETRIEVE_DATATHONS,
  RETRIEVE_DATATHONS_FAILURE,
  RETRIEVE_DATATHONS_SUCCESS,
  RETRIEVE_DATATHON,
  RETRIEVE_DATATHON_FAILURE,
  RETRIEVE_DATATHON_SUCCESS
} from "../constants/actionTypes"

const initialState = {
  datathons: {},
  retrieval: {
    isFetching: false,
    errorMsg: null
  },
  creation: {
    isProcessing: false,
    errors: null
  }
}
  
const datathonsReducer = (state = initialState, action) => {
  switch (action.type) {
    case RETRIEVE_DATATHONS:
      return { ...state, retrieval: {isFetching: true, errorMsg: null} }
    case RETRIEVE_DATATHONS_SUCCESS:
      return {
        ...state,
        datathons: action.datathons.reduce(
          (acc, cur) => ({ ...acc, [cur.id]: cur }), {}),
        retrieval: {
          isFetching: false,
          errorMsg: null
        }
      }
    case RETRIEVE_DATATHONS_FAILURE:
      return {
        ...state,
        retrieval: { isFetching: false, errorMsg: action.errorMsg }
      }
    case RETRIEVE_DATATHON:
      return { ...state, isFetching: true }
    case RETRIEVE_DATATHON_SUCCESS:
      return {
        ...state,
        retrieval: { isProcessing: false, errors: null },
        datathons: {
          ...state.datathons,
          [action.datathon.id]: action.datathon,
        }
      }
    case RETRIEVE_DATATHON_FAILURE:
      return {
        ...state,
        retrieval: { isProcessing: false, errors: action.errors }
      }
    case ADD_DATATHON:
      return { ...state, creation: { isProcessing: true, errors: null }}
    case ADD_DATATHON_SUCCESS:
      return {
        ...state,
        creation: { isProcessing: false, errors: null },
        datathons: {
          ...state.datathons,
          [action.datathon.id]: action.datathon,
        }
      }
    case ADD_DATATHON_FAILURE:
      return {
        ...state,
        creation: {
          isProcessing: false,
          errors: action.errors
        }
      }
    default:
      return state
  }
}

export default datathonsReducer