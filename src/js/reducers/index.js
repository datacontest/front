import { combineReducers } from 'redux'
import authReducer from './auth'
import datathonsReducer from './datathons'


const rootReducer = combineReducers({
  datathonsReducer,
  authReducer
})

export default rootReducer
