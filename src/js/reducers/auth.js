import {
  LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE,
  LOGOUT
} from '../constants/actionTypes'

const initialState = {
  isFetching: false,
  errorMsg: null,
  isAuthenticated: false,
  user: {
    username: null,
    id: null,
  }
}

const authReducer = (state = initialState, action) => {
  switch(action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isFetching: true, isAuthenticated: false, errorMsg: null, username: null
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isFetching: false, errorMsg: null, isAuthenticated: true,
        user: action.user
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isFetching: false, errorMsg: action.errorMsg, isAuthenticated: false,
        user: {
          username: null,
          id: null
        }
      };
    case LOGOUT:
      return {
        ...state, isFetching: false, errorMsg: null, isAuthenticated: false, username: null
      }
    default:
      return state;
  }
}

export default authReducer
