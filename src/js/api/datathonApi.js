import BaseApi from './baseApi'

class DatathonApi extends BaseApi {
  static getAll() {
    return fetch(`${this.baseUrl()}/datathons/`, {
      headers: {'Content-Type': 'application/json'}
    }).then(response => response.json())
  }

  static get(id) {
    return fetch(`${this.baseUrl()}/datathons/${id}`, {
      headers: {'Content-Type': 'application/json'}
    }).then(response => response.json())
  }

  static add({ title, description, metric, dataset_description, dataset_target_variable_name, dataset_training_set, dataset_test_set, dataset_validation_set }) {
    function handleResponse(response) {
      return response.json().then(data => {
        if (!response.ok) {
          if ([400, 401].includes(response.status)) {
            return Promise.reject(data)
          }

          const error = (data && data.error) || response.statusText;
          return Promise.reject(error);
        }

        return data;
      })
    }

  const token = localStorage.getItem('jwt_token')
  let formData = new FormData()
  formData.append('title', title)
  formData.append('description', description)
  formData.append('metric', metric)

  formData.append('dataset.description', dataset_description)
  formData.append('dataset.target_variable_name', dataset_target_variable_name)
  formData.append('dataset.training_set', dataset_training_set)
  formData.append('dataset.test_set', dataset_test_set)
  formData.append('dataset.validation_set', dataset_validation_set)

  return fetch(`${this.baseUrl()}/datathons/`, {
      headers: {
        'Authorization': `JWT ${token}`
      },
      method: 'POST',
      body: formData
    }).then(handleResponse)
  }
}

export default DatathonApi
