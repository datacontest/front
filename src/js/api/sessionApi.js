import BaseApi from './baseApi'
// import fetch from 'cross-fetch'

class SessionApi extends BaseApi {
  static login(username, password) {
    /*
      Returns an object with token and username if the user
      is successfully authenticated against the API
    */
    function handleResponse(response) {
      return response.json().then(data => {
        if (!response.ok) {
          console.log("Response not ok")
          console.log(response.status)
          console.log(response.statusText)
          if (response.status === 401) {
            // auto logout if 401 response returned from api
            logout();
            location.reload(true);
          }

          const error = (data && data.error) || response.statusText;
          return Promise.reject(error);
        }

        return data;
      });
    }

    return fetch(`${this.baseUrl()}/api-token-auth/`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ username, password })
    }).then(handleResponse)
      .then(data => {
        console.log("Retrieved", data)
        return {
          token: data.token,
          user: {
            username: data.user.username,
            user_id: data.user.id
          }
        }
      })
  }

  static refreshToken(token) {
    return fetch(`${this.baseUrl()}/api-token-refresh/`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ token })
    })
  }
}
  
export default SessionApi
